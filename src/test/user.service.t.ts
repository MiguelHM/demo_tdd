import { test, expect, describe, beforeAll } from "vitest";
import { UserService } from "../modules/user/user.service";

// TEST UNITARIOS
describe("USER SERVICE", () => {
  let userService: UserService;

  beforeAll(() => {
    userService = new UserService();
  });

  test("1 - TEST DEMO", () => {
    expect(true).toBe(true);
  });

  test("2 - Debe ser posible crear un usuario", async () => {
    const usuarioCreado = await userService.create({
      name: "Pepe",
      username: "juan23",
      email: "test@test.com",
    });

    expect(usuarioCreado).toHaveProperty("email");
    expect(usuarioCreado.name).toBe("Pepe");
  });

  test("3 - No debe crear un usuario con el mismo correo", async () => {
    // refactor de la instancia de UserService
    // const userService = new UserService();

    await userService.create({
      name: "Mario",
      username: "mariobross",
      email: "mario@mario.com",
    });

    /*     const usuarioCreado = await userService.create({
      name: "Luiggi",
      username: "Luiggi",
      email: "mario@mario.com",
    }); */

    // expect(usuarioCreado).toHaveProperty("id");

    /*          expect(async()=>{
      await userService.create({
        name: "Luiggi",
        username: "Luiggi",
        email: "mario@mario.com",
      }). */

    expect(async () => {
      await userService.create({
        name: "Luiggi",
        username: "Luiggi",
        email: "mario@mario.com",
      });
    }).rejects.toThrow("User already exists");
  });
});
