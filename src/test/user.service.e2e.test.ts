import { test, expect, describe, beforeAll } from "vitest";
import request from "supertest";
import http from "http";

import { app } from "../app";

// TEST DE INTEGRACIÓN

describe("User rutas", () => {
  test("Debe Crear usuarios", async () => {
    await app.ready();
    await request(app.server)
      .post("/users")
      .set("Authorization", "Bearer TOKEN_FALSO")
      .send({
        name: "Juan",
        id: "123",
      })
      .expect(200);
  });

  test.only("Debe Crear usuarios", async () => {
    await app.ready();
    await request(app.server)
      .post("/users")
      .set("Authorization", "Bearer TOKEN_FALSO")
      .send({
        name: "Juan",
        id: "123",
      })
      .expect(200)
      .then(({ body: usuarioCreado }) => {
        expect(usuarioCreado).toHaveProperty("id");
      });
  });
});
