import { expect, test } from "vitest";
import suma from "./suma";

test("TEST DE LA FUNCIÓN SUMA", () => {
  expect(suma(1, 2)).toBe(3);
  expect(suma(2, 2)).toBe(4);
  expect(suma(2, 3)).toBe(5);
});

test("TEST DE LA RESTA", () => {
  expect(5 - 2).toBe(3);
});
